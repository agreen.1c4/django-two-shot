from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

USER_MODEL = settings.AUTH_USER_MODEL

class ExpenseCategory(models.Model):
    name = models.CharField(max_length = 50)
    owner = models.ForeignKey(
        User,
        related_name="categories",
        on_delete=models.CASCADE,
        )
    def __str__(self):
        return f"{self.name}"

class Account(models.Model):
    name = models.CharField(max_length = 100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User,
        related_name="accounts",
        on_delete=models.CASCADE,
        )
    
    def __str__(self):
        return f"{self.name}"
    

class Receipt(models.Model):
    vendor = models.CharField(max_length = 200)
    total = models.DecimalField(max_digits=10, decimal_places=3, default=0.00)
    tax = models.DecimalField(max_digits=10, decimal_places=3, default=0.00)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User,
        related_name="receipts",
        on_delete=models.CASCADE,
        )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
        )        
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
        )
    
    
    
    
    
