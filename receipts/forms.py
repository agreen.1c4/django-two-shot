# receipts/forms.py
from django import forms
from django.forms import ModelForm, DateInput, NumberInput
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']
        widgets = {
            'total': NumberInput(attrs={'step':0.25}),
            'date': DateInput(attrs={'type':'date'}),
        }

class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            'name',
        ]
        
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            'name',
            'number',
        ]